<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\UnitRumah;

class ProductController extends Controller
{
    function CreateUnit (Request $request){
        DB::beginTransaction();
        try{

            $this->validate($request, [
                'no_rumah' => 'required',
                'harga_rumah' => 'required',
                'luas_tanah' => 'required'
            ]);

            $kavling = $request->input('kavling');
            $blok = $request->input('blok');
            $no_rumah = $request->input('no_rumah');
            $harga_rumah = $request->input('harga_rumah');
            $luas_tanah = $request->input('luas_tanah');
            $luas_bangunan = $request->input('luas_bangunan');


            $ur = new UnitRumah;
            $ur->kavling = $kavling;
            $ur->blok = $blok;   
            $ur->no_rumah = $no_rumah;
            $ur->harga_rumah = $harga_rumah;
            $ur->luas_tanah = $luas_tanah;
            $ur->luas_bangunan = $luas_bangunan;
            $ur->save();

            DB::commit();
            return response()->json(["message" => "Success !!"], 200);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
        }

    }

}
